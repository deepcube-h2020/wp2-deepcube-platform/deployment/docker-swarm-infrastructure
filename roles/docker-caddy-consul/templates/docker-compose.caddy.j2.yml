version: '3.8'

# used for specific settings you have outside of your docker config
# ex: proxies to external servers, storage configuration...
# remove this block entirely if not needed (Only used for Docker Swarm)
configs:
  Caddyfile:
    name: "{{ caddy_consul_stack_name }}_Caddyfile-{{ caddy_consul_timestamp_deploy }}"
    file: "./Caddyfile"

services:

  consul:
    image: "consul:{{ caddy_consul_consul_version }}"
    hostname: "{% raw %}{{ .Node.Hostname }}{% endraw %}"
    command: agent -server -client=0.0.0.0 -bootstrap-expect={{ consul_replicas }} {% for consul_host_name in caddy_consul_agent_hosts %}-retry-join={{ consul_host_name }} {% endfor %}-raft-protocol=3 -ui
    healthcheck:
      test: "consul info | grep 'health_score = 0'"
    volumes:
      - consul-data-replica:/consul/data
    environment:
      - CONSUL_BIND_INTERFACE=eth0
      - 'CONSUL_LOCAL_CONFIG={"leave_on_terminate": true, "acl": {"enabled": true, "tokens": {"master": "{{ consul_acl_master_token }}", "agent": "{{ consul_acl_master_token }}"}}}'
    networks:
      internal:
      {{ caddy_network }}:
    deploy:
      mode: replicated
      replicas: {{ consul_replicas }}
      placement:
        constraints:
          - node.role == manager
        preferences:
          - spread: node.id
        max_replicas_per_node: 1
      restart_policy:
        condition: any
        delay: 15s
        window: 120s
{% if consul_max_attempts > 0 %}
        max_attempts: {{ consul_max_attempts }}
{% endif %}
      labels:
        caddy: "{{ consul_ui_domain }}"
        # enable access log
        caddy.log: ""

        # hsts headers (see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security)
        caddy.1_header.Strict-Transport-Security: "\"max-age=63072000; includeSubDomains; preload\""

        # security labels
        caddy.1_header.-Server: ""
        caddy.1_header.-X-Powered-By: ""
        caddy.1_header.Permissions-Policy: "\"accelerometer=(), interest-cohort=(), camera=(), geolocation=(), gyroscope=(), magnetometer=(), microphone=(), payment=(), usb=()\"" # see best practices https://github.com/w3c/webappsec-permissions-policy/blob/master/features.md, https://developer.mozilla.org/en-US/docs/Web/HTTP/Feature_Policy
        caddy.1_header.Referrer-Policy: "same-origin"
        caddy.1_header.X-Content-Type-Options: "nosniff"
        caddy.1_header.X-Frame-Options: "DENY"
        caddy.1_header.X-XSS-Protection: "\"1; mode=block\""
        caddy.1_header.Content-Security-Policy: "\"default-src 'self'; script-src 'self' 'unsafe-inline'; img-src 'self' data:; style-src 'self' 'unsafe-inline' data:; frame-ancestors 'self'; frame-src 'self'; object-src 'none';\"" # https://csp-evaluator.withgoogle.com/ content security policy


        # ip filter auth & reverse proxy conf
        caddy.2_@ipfilter.remote_ip: "{{ trusted_backend_ips|join(' ') }} 127.0.0.1"

        # apply ip filter
        caddy.3_handle: "@ipfilter"
        # basic auth
        caddy.3_handle.1_basicauth: "/*" # route matcher to protect
{% for authuser in backend_users %}
{% set user_loc = authuser.split(':') %}
        caddy.3_handle.1_basicauth.{{ user_loc[0] }}: "{{ user_loc[1] | replace('$', '$$') }}" # because '$' is evaluated, remember to escape it
{% endfor %}
        # try to match filter for a reverse proxy to upstream (eventually add load balancing etc)
        caddy.3_handle.2_reverse_proxy: "{% raw %}{{upstreams 8500}}{% endraw %}"
        caddy.3_handle.2_reverse_proxy.lb_policy: "round_robin"
        caddy.3_handle.2_reverse_proxy.health_uri: "/ui/"
        caddy.3_handle.2_reverse_proxy.health_interval: "5s"

        # or reject
        caddy.4_respond: "`Origin not allowed` 403"

  caddy:
    image: "docker.gael-systems.com/caddy-consul-docker:{{ caddy_version }}"
    command: caddy docker-proxy --caddyfile-path=/etc/caddy/Caddyfile
    healthcheck:
      test: "curl --silent --fail http://localhost:9500/metrics > /dev/null || exit 1"
    sysctls:
      net.core.somaxconn: {{ caddy_max_connections_sysctl }}
    configs:
      - source: Caddyfile
        target: /etc/caddy/Caddyfile
    ports:
      # read the client IP in your applications/stacks using the X-Forwarded-For
      #  or X-Real-IP headers provided by caddy
      - target: 80
        published: 80
        mode: host
      - target: 443
        published: 443
        mode: host
      - target: {{ caddy_metrics_port }}
        published: {{ caddy_metrics_port }}
        mode: host
    volumes:
      - caddy-data:/data
      - /var/run/docker.sock:/var/run/docker.sock
    environment:
      - "CADDY_INGRESS_NETWORKS={{ caddy_network }}"
      - CADDY_DOCKER_LABEL_PREFIX=caddy
      - CADDY_DOCKER_MODE=standalone
      - CADDY_DOCKER_PROCESS_CADDYFILE=true
      - CADDY_DOCKER_PROXY_SERVICE_TASKS=true
    deploy:
      mode: global
      placement:
        constraints:
          - node.role == manager
        preferences:
          - spread: node.id
      restart_policy:
        condition: any
        delay: 15s
        window: 30s
{% if caddy_max_attempts > 0 %}
        max_attempts: {{ caddy_max_attempts }}
{% endif %}
      resources:
        reservations:
          cpus: "0.1"
          memory: 200M
    logging:
      options:
        # this tag is used by fail2ban using `journalctl CONTAINER_TAG=caddy-consul`
        tag: "caddy-consul"
    networks:
      - internal
      - "{{ caddy_network }}"

  # do not expose docker socket directly
  docker-proxy:
    image: rancher/socat-docker
    networks:
      - internal
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    deploy:
      # replicas + 1 which is the leader
      mode: global
      placement:
        constraints:
          - node.role == manager
        preferences:
          - spread: node.id
      restart_policy:
        condition: any
        delay: 15s
        window: 30s

volumes:
  consul-data-replica:
  caddy-data:

networks:
  internal:
    driver: overlay
{% if caddy_internal_network_encryption_enabled %}
    driver_opts:
      encrypted: ""
{% endif %}
  {{ caddy_network }}:
    external: true
...