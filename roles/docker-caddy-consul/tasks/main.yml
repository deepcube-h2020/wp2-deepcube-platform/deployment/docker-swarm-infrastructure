---
# This role setup a reverse-proxy/load balancer for the swarm using caddy and consul

- name: Verify Consul ACL Token is defined and not empty
  assert:
    that: >
      consul_acl_master_token is defined and consul_acl_master_token|length
    msg: "consul_acl_master_token must be defined an not empty"

- name: Verify Consul Caddy encryption key is defined and 32 caracters long
  assert:
    that: >
      caddy_consul_certificates_aes_key is defined and (caddy_consul_certificates_aes_key|length == 32)
    msg: "caddy_consul_certificates_aes_key must be defined an 32 caracters long (current size = caddy_consul_certificates_aes_key|length)"

- name: Verify Consul replica count is compatible with the raft consensus algorithm
  assert:
    that: >
      consul_replicas|int == 1 or consul_replicas|int == 3 or consul_replicas|int == 5 or consul_replicas|int == 7
    msg: "consul_replicas must either be 1,3,5,7 to be compatible with the raft algorithm consensus (current value = {{ consul_replicas }})"


- name: Create caddy-public network with encryption
  docker_network:
    name: "{{ caddy_network }}"
    driver: overlay
    driver_options:
      encrypted: ""
  when: caddy_internal_network_encryption_enabled is true

- name: Create caddy-public network without encryption
  docker_network:
    name: "{{ caddy_network }}"
    driver: overlay
  when: caddy_internal_network_encryption_enabled is false

## caddy-controller network creation
- name: Create caddy-controller network with encryption
  docker_network:
    name: "caddy_controller"
    driver: overlay
    driver_options:
      encrypted: ""
  when: caddy_internal_network_encryption_enabled is true and caddy_use_standalone_mode is false
  register: caddy_controller_encrypted_infos

- set_fact: caddy_controller_infos="{{caddy_controller_encrypted_infos}}"
  when: caddy_internal_network_encryption_enabled is true and caddy_use_standalone_mode is false

- name: Create caddy-controller network without encryption
  docker_network:
    name: "caddy_controller"
    driver: overlay
  when: caddy_internal_network_encryption_enabled is false and caddy_use_standalone_mode is false
  register: caddy_controller_unencrypted_infos

- set_fact: caddy_controller_infos="{{caddy_controller_unencrypted_infos}}"
  when: caddy_internal_network_encryption_enabled is false and caddy_use_standalone_mode is false

## end of caddy controller network creation

- name: Ensure caddy directories exists
  file:
    state: directory
    path: "{{ item }}"
  with_items:
    - "{{ caddy_compose_file_dest_directory }}"

- name: Register deployment timestamp
  set_fact:
    caddy_consul_timestamp_deploy: "{{ ansible_date_time.epoch }}"

- name: Copy Caddyfile config
  template:
    src: templates/Caddyfile.j2
    dest: "{{ caddy_config_file_dest }}"
    mode: u=rw,g=,o=

- name: Copy caddy-Consul stack
  template:
    src: "templates/{{ caddy_compose_file_source_name }}"
    dest: "{{ caddy_compose_file_dest }}"
    mode: u=rw,g=,o=

- name: Fail2ban for caddy
  import_tasks: fail2ban.yml

- name: Configure stack firewall
  import_tasks: firewall.yml

# always redeploy permits to have always up to date
- name: Deploy caddy
  docker_stack:
    state: present
    prune: yes
    name: caddy-consul
    compose:
      - "{{ caddy_compose_file_dest }}"
  notify: docker prune
...