version: "3.7"

services:
  portainer:
    image: portainer/portainer-ce:latest
    # see https://documentation.portainer.io/v2.0/deploy/initial/
    command: -H tcp://tasks.agent:9001 --tlsskipverify --admin-password='{{ portainer_admin_password| replace('$', '$$') }}'
    volumes:
      - portainer-data:/data
    networks:
      - agent-network
      - {{ caddy_network }}
    deploy:
      placement:
        constraints:
          - node.role == manager
          - node.labels.portainer.portainer-data == true
      restart_policy:
        condition: any
        delay: 5s
        window: 45s
{% if portainer_max_attemps > 0 %}
        max_attempts: {{ portainer_max_attemps }}
{% endif %}
      labels:
        caddy: "{{ portainer_domain }}"
        # enable access log
        caddy.log: ""

        # hsts headers (see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security)
        caddy.1_header.Strict-Transport-Security: "\"max-age=63072000; includeSubDomains; preload\""

        # security labels
        caddy.1_header.-Server: ""
        caddy.1_header.-X-Powered-By: ""
        caddy.1_header.Permissions-Policy: "\"accelerometer=(), camera=(), interest-cohort=(), geolocation=(), gyroscope=(), magnetometer=(), microphone=(), payment=(), usb=()\"" # see best practices https://github.com/w3c/webappsec-permissions-policy/blob/master/features.md, https://developer.mozilla.org/en-US/docs/Web/HTTP/Feature_Policy
        caddy.1_header.Referrer-Policy: "same-origin"
        caddy.1_header.X-Content-Type-Options: "nosniff"
        caddy.1_header.X-Frame-Options: "DENY"
        caddy.1_header.X-XSS-Protection: "\"1; mode=block\""
        caddy.1_header.Content-Security-Policy: "\"default-src 'self'; script-src 'self'; img-src 'self' data: {{ portainer_csp_img_src_additional }}; style-src 'self' 'unsafe-inline'; font-src 'self' data:; frame-ancestors 'self'; frame-src 'self'; object-src 'none';\"" # https://csp-evaluator.withgoogle.com/ content security policy

        # ip filter auth & reverse proxy conf
        caddy.2_@ipfilter.remote_ip: "{{ trusted_backend_ips|join(' ') }} 127.0.0.1"

        # apply ip filter
        caddy.3_handle: "@ipfilter"

        # try to match filter for a reverse proxy to upstream (eventually add load balancing etc)
        caddy.3_handle.2_reverse_proxy: "{% raw %}{{upstreams 9000}}{% endraw %}"
        caddy.3_handle.2_reverse_proxy.lb_policy: "round_robin"
        caddy.3_handle.2_reverse_proxy.health_uri: "/"
        caddy.3_handle.2_reverse_proxy.health_interval: "5s"

        # or reject
        caddy.4_respond: "`Origin not allowed` 403"

      resources:
        limits:
          cpus: '0.2'
          memory: 256M
        reservations:
          cpus: '0.1'
          memory: 128M

  agent:
    image: portainer/agent:latest
    environment:
      AGENT_CLUSTER_ADDR: tasks.agent
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - /var/lib/docker/volumes:/var/lib/docker/volumes
    networks:
      - agent-network
    deploy:
      mode: global
      placement:
        constraints:
          - node.platform.os == linux
      restart_policy:
        condition: any
        delay: 5s
        window: 30s
      resources:
        limits:
          cpus: '0.10'
          memory: 64M
        reservations:
          cpus: '0.05'
          memory: 32M

networks:
  agent-network:
    driver: overlay
    attachable: true
{% if portainer_internal_network_encryption_enabled %}
    driver_opts:
      encrypted: ""
{% endif %}
  {{ caddy_network }}:
    external: true

volumes:
  portainer-data:
    driver: local
