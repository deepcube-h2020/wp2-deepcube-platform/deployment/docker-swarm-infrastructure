# Terraform Settings
terraform {
  required_version = ">= v1.0.0"

  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
    }
  }
}


# Set default values
locals {
  flavor_name             = lookup(var.instance_defaults, "flavor_name", "d2-2")
  image_id                = lookup(var.instance_defaults, "image_name", "0000-0000-00000-0000")
  private_network_name    = lookup(var.instance_defaults, "private_network_name", "network_1")
  key_pair                = coalesce(var.keypair_name,      lookup(var.instance_defaults, "key_pair",  "default-keypair"))
  security_groups         = lookup(var.instance_defaults, "security_groups", [ "default" ])
  region                  = lookup(var.instance_defaults, "region", "default")
  boot_device_volume_size = lookup(var.instance_defaults, "boot_device_volume_size", 0)
  volume_type             = lookup(var.instance_defaults, "volume_type", null)
}

resource "openstack_compute_instance_v2" "openstack_instance" {
    name            = var.instance_spec["name"]
    flavor_name     = lookup(var.instance_spec, "flavor_name", local.flavor_name)
    image_id        = lookup(var.instance_spec, "image_id", local.image_id)
    key_pair        = lookup(var.instance_spec, "key_pair", local.key_pair)
    security_groups = lookup(var.instance_spec, "security_groups", local.security_groups)
    region          = lookup(var.instance_spec, "region", local.region)
    
    # Public network
    network {
      name = "Ext-Net"
    }
    
    # Networks
    dynamic "network" {
      for_each = try(var.instance_spec.networks, [])
      content {
        name        = network.value["name"]
        fixed_ip_v4 = try(network.value["fixed_ip_v4"], null)
        fixed_ip_v6 = try(network.value["fixed_ip_v6"], null)
        floating_ip = try(network.value["floating_ip"], null)
        mac         = try(network.value["mac"], null)
        port        = try(network.value["port"], null)
        uuid        = try(network.value["uuid"], null)
      }
    }
    
    block_device {
      uuid                  = lookup(var.instance_spec, "image_id", local.image_id)
      source_type           = "image"
      destination_type      = "local"
      boot_index            = 0
      delete_on_termination = true
      volume_size           = lookup(var.instance_spec, "boot_device_volume_size", local.boot_device_volume_size)
    }

    # Block devices
    dynamic "block_device" {
      for_each = try(var.instance_spec.block_devices, [])
      content {
        volume_type            = try(block_device.value["volume_type"], local.volume_type)
        boot_index             = try(block_device.value["boot_index"], 1)
        delete_on_termination  = try(block_device.value["delete_on_termination"], true)
        destination_type       = try(block_device.value["destination_type"], "volume")
        source_type            = try(block_device.value["source_type"], "blank")
        volume_size            = try(block_device.value["volume_size"], 0)
        guest_format           = try(block_device.value["guest_format"], "ext4")
      }
    }
}

# volume creation
resource "openstack_blockstorage_volume_v2" "volumes" {
    count       = length(var.instance_spec.additional_volumes)

    name        = lookup(var.instance_spec.additional_volumes[count.index], "name", null)                           
    region      = lookup(var.instance_spec.additional_volumes[count.index], "region", openstack_compute_instance_v2.openstack_instance.region)
    description = lookup(var.instance_spec.additional_volumes[count.index], "description", null) 
    volume_type = lookup(var.instance_spec.additional_volumes[count.index], "volume_type", local.volume_type)
    size        = lookup(var.instance_spec.additional_volumes[count.index], "size", null)    

    metadata    = { 
        attached_mode: "rw"
        readonly: "False"
        instance_owner: var.instance_spec.name 
    }
}

# attach the volume to the instances
resource "openstack_compute_volume_attach_v2" "volume_attachments" {
    count         = length(var.instance_spec.additional_volumes)

    instance_id   = openstack_compute_instance_v2.openstack_instance.id
    volume_id     = openstack_blockstorage_volume_v2.volumes[count.index].id

    device        = lookup(var.instance_spec.additional_volumes[count.index], "device", null)                           

    region        = lookup(var.instance_spec.additional_volumes[count.index], "region", openstack_compute_instance_v2.openstack_instance.region)
}

output "instance" {
  value     = openstack_compute_instance_v2.openstack_instance
  sensitive = false
}

output "volumes" {
  value     = openstack_blockstorage_volume_v2.volumes
  sensitive = false
}

output "volume_attachments" {
  value     = openstack_compute_volume_attach_v2.volume_attachments
  sensitive = false
}