# list of variables

variable "PROJECT_NAME" {
  description = "Project name used as a global variable"
  type = string
  default = "deepcube_swarm"
}

# Instances definition
variable "SWARM_INSTANCES_DEFAULT" {
    description = "Swarm instances default values"
    type = any
    default = {
      flavor_name           = "d2-4"
      image_name            = "Debian 11"
      ssh_user              = "debian"
      instance_prefix       = "swarm_node"
      security_groups       = [ "default" ]
    }
}

variable "SWARM_INSTANCES_SPECIFICATIONS" {
    description = "Swarm specification values per instance"
    type = list(any)
    default = [
      { 
        name = "hell01.dv", # name of server, must be unique
        image_id = "ea5719f6-d2ee-4e99-9696-e846a75b4b8e" # retrieve the id from openstack image list
        networks = [ # attach to different networks
              {
                name = "network_deepcube_swarm_inet" 
                fixed_ip_v4   = "192.168.30.12",
              },
              {
                name = "network_deepcube_global_inet" 
                fixed_ip_v4   = "10.10.10.10",
              },
        ],
        # add a network volume directly to the instance
        block_devices  = [
            { 
                volume_size  = 10
                guest_format = "xfs"
            }
        ],
        # additional volumes to attach
        additional_volumes = [
          {
            name = "my-srv",
            size = 10,
            description = "default /srv volume of the instance",
            volume_type = "high-speed"
          },
          {
            name = "other-volume",
            size = 10,
          },
          {
            name = "docker_partition",
            size = 30,
            description = "docker_partition of the instance",
            volume_type = "high-speed"
          },
        ]
        region = "GRA9",
      },
      {
        name = "hell02.dv",
        image_id = "8550744c-a173-4744-af90-f733653621eb"
        networks = [
              {
                name = "network_deepcube_swarm_inet" 
                fixed_ip_v4   = "192.168.30.13",
              },
              {
                name = "network_deepcube_global_inet" 
                fixed_ip_v4   = "10.10.10.11",
              },
        ],
        block_devices  = [
            { 
                volume_size  = 10
                guest_format = "xfs"
            },
        ],
        additional_volumes = [
          {
            name = "my-volume",
            size = 45,
          },
          {
            name = "docker_partition",
            size = 30,
            description = "docker_partition of the instance",
            volume_type = "high-speed"
          },
        ],
        region = "GRA5",
      },
      {
        name = "hell03.dv",
        image_id = "95cabe2b-4e20-496d-ab36-f8a9551358f6"
        networks = [
              {
                name = "network_deepcube_swarm_inet" 
                fixed_ip_v4   = "192.168.30.14",
              },
              {
                name = "network_deepcube_global_inet" 
                fixed_ip_v4   = "10.10.10.12",
              },
        ],
        block_devices  = [
            { 
                volume_size  = 10
                guest_format = "xfs"
            }
        ],
        additional_volumes = [
          {
            name = "docker_partition",
            size = 30,
            description = "docker_partition of the instance",
            volume_type = "high-speed"
          }
        ],
        region = "GRA7",
      }
    ]
}


# End of instances definitions

# ssh-rsa ONLY, ssh-ed25519 is not yet supported by OVH / ecdsa not trusted
variable "BASE_DEFAULT_SSH_PUBLIC_KEY" {
    description = "Default SSH Key to be distributed on the instances"
    default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDb/KjvfOw1nmozU4eMdXjb2TDqBHrHfdajWR/6OyYj44U3yQrTRFexmc8WiighGh5p9pGtWur8DjeNQ31ErViYhVg12Ie07PrCRe2cACG/ZNReqPJRS3LP2MChI9VKTctF/l4aST6jCoylHL9yWB006UBp3ubk4EhvgKKfI33maDgOtYEm9XDDikQZSciuFwb1yPpbM1ikB21hhOd/OlwFLV+J/kPYb61hQEOC5tdYyO5zz8V5bf2bl6dTqsV14ljt8U6g/vVFWuErUWF62prz4vpMgd0Frdn1JzUor9Zja4BdY6Nm9Q8pn4Abfhqt1P67yeAL+SP+C+s3fCy0UHUMdk4PQ8dvm5nMhdXBw+UoR0RR7wUSrsebTboY1bJ1GbbU3u4tnmljlG2XjZnbdMELBbWG3VJ2Gh75Hdzzt32ny4VunI1Dcu0T6BGkholPTV7HSIw1XkWAWbVewaJtgvhAZh6d/D+bd7lPelHndS4ErpGjV3obBUBuKqW4AQSZcChXl/Jp+LudXqPPGGfb2cAJPMMWIz6F3P/oJhlHNDg1T8Bk6vfwhRMQhuwqaxdWQQlUWBzwlel+/HpFmssAFkxeDD5lsDRMm70Miazqa998EJ4rl0U+UCYeN+afpWzJqi1GcNVP96CXZ/1ezh/RTXnWAaRKpDqGBu7sERq8oyIaJw== leo-gad.journel@gael.fr"
    type = string
}

# configure the cluster
variable "BASE_DEFAULT_DNS_SERVERS" {
    default = ["1.1.1.1","1.0.0.1"]
    type = list(string)
}

# DeepCube Network configuration
variable "CLUSTER_SWARM_PRIVATE_NETWORK_CIDR" {
    description =  "cidr of the openstack implementation of the private network"
    default = "192.168.30.0/24"
    type = string
}

variable "CLUSTER_SWARM_PRIVATE_NETWORK_NAME" {
    description =  "name of the openstack implementation of the private network"
    default = "network_deepcube_swarm_inet"
    type = string
}

variable "NETWORK_PROVIDER_TYPE" {
    default = "ovh" # can be: openstack, ovh
    type = string
    description = "Type of the network provider: can be ovh, openstack. Only OVH allows multiple regions."
}

variable "NETWORKS_CLUSTER_REGIONS" {
     description = "List of the region to deploy the network. (null value will use a computed list derivated from the instances)"
     type = set(string)
     default = null
}

variable "OVH_NETWORKS_VLAN_ID" {
     description = "Vlan id of OVH operated network"
     type = number
     default = 1202
}

# global internal network operated externally 
variable "CLUSTER_INTERNAL_PRIVATE_NETWORK_NAME" {
    default = "deepcube_internal_network_global"
    type = string
}