# Setup instances of the cluster

# create the SSH keypair
resource "openstack_compute_keypair_v2" "initial_op_keypair_all" {
  for_each = local.CLUSTER_REGIONS

  name = local.KEYPAIR_NAME

  public_key = var.BASE_DEFAULT_SSH_PUBLIC_KEY

  region = each.key
}

module "deepcube_swarm_cluster" {
    count                   = length(var.SWARM_INSTANCES_SPECIFICATIONS)
    source                  = "./modules/openstack-instance"

    depends_on = [openstack_networking_subnet_v2.ovh_subnetwork_deepcube_swarm_inet, 
                  openstack_networking_subnet_v2.os_subnetwork_deepcube_swarm_inet]

    
    # default values for all instances of the cluster
    instance_defaults = var.SWARM_INSTANCES_DEFAULT

    keypair_name = local.KEYPAIR_NAME


    # override default values from above
    instance_spec = var.SWARM_INSTANCES_SPECIFICATIONS[count.index]
}