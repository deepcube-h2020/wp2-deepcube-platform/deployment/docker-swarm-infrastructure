# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/local" {
  version = "2.1.0"
  hashes = [
    "h1:EYZdckuGU3n6APs97nS2LxZm3dDtGqyM4qaIvsmac8o=",
    "zh:0f1ec65101fa35050978d483d6e8916664b7556800348456ff3d09454ac1eae2",
    "zh:36e42ac19f5d68467aacf07e6adcf83c7486f2e5b5f4339e9671f68525fc87ab",
    "zh:6db9db2a1819e77b1642ec3b5e95042b202aee8151a0256d289f2e141bf3ceb3",
    "zh:719dfd97bb9ddce99f7d741260b8ece2682b363735c764cac83303f02386075a",
    "zh:7598bb86e0378fd97eaa04638c1a4c75f960f62f69d3662e6d80ffa5a89847fe",
    "zh:ad0a188b52517fec9eca393f1e2c9daea362b33ae2eb38a857b6b09949a727c1",
    "zh:c46846c8df66a13fee6eff7dc5d528a7f868ae0dcf92d79deaac73cc297ed20c",
    "zh:dc1a20a2eec12095d04bf6da5321f535351a594a636912361db20eb2a707ccc4",
    "zh:e57ab4771a9d999401f6badd8b018558357d3cbdf3d33cc0c4f83e818ca8e94b",
    "zh:ebdcde208072b4b0f8d305ebf2bfdc62c926e0717599dcf8ec2fd8c5845031c3",
    "zh:ef34c52b68933bedd0868a13ccfd59ff1c820f299760b3c02e008dc95e2ece91",
  ]
}

provider "registry.terraform.io/hashicorp/template" {
  version = "2.2.0"
  hashes = [
    "h1:94qn780bi1qjrbC3uQtjJh3Wkfwd5+tTtJHOb7KTg9w=",
    "zh:01702196f0a0492ec07917db7aaa595843d8f171dc195f4c988d2ffca2a06386",
    "zh:09aae3da826ba3d7df69efeb25d146a1de0d03e951d35019a0f80e4f58c89b53",
    "zh:09ba83c0625b6fe0a954da6fbd0c355ac0b7f07f86c91a2a97849140fea49603",
    "zh:0e3a6c8e16f17f19010accd0844187d524580d9fdb0731f675ffcf4afba03d16",
    "zh:45f2c594b6f2f34ea663704cc72048b212fe7d16fb4cfd959365fa997228a776",
    "zh:77ea3e5a0446784d77114b5e851c970a3dde1e08fa6de38210b8385d7605d451",
    "zh:8a154388f3708e3df5a69122a23bdfaf760a523788a5081976b3d5616f7d30ae",
    "zh:992843002f2db5a11e626b3fc23dc0c87ad3729b3b3cff08e32ffb3df97edbde",
    "zh:ad906f4cebd3ec5e43d5cd6dc8f4c5c9cc3b33d2243c89c5fc18f97f7277b51d",
    "zh:c979425ddb256511137ecd093e23283234da0154b7fa8b21c2687182d9aea8b2",
  ]
}

provider "registry.terraform.io/ovh/ovh" {
  version     = "0.13.0"
  constraints = "0.13.0"
  hashes = [
    "h1:SzUdeCY+ImQb5kGA61YzeqOp4lhKIgX0W7vhgVmcLCA=",
    "zh:1abeb108cb26aa5063ea0288f7ddb8bf20bdb93f81323b88f538e2067046c1bc",
    "zh:29fdcdd09d58482d290fc23e3539d4b4c4e6245ad6b6d1ed4c7c1c36320a1135",
    "zh:371a20ad17050357a175bd271f734af5a75f169ca4216749842ffc67d505d197",
    "zh:3c735184f0fe7ebcacd2aa7798be7d324c863346736129011479c10b315f4bd0",
    "zh:487ebdfbd5a8a9ae0d49e568be1f0e0f3df47441359ddc706aa18a545fd00276",
    "zh:4a88c7659c961803b48c423bd37098918b5e5a9509b034bb105f01643b22de3b",
    "zh:55ec093da7854a72e7fae7b21e730c01f87505935bfddcf926ef7a995ca7c64a",
    "zh:8ebaeab119673aa1ab6084225e4d24077cc454b781fd11c7d4a05258c53e2a25",
    "zh:be4448ed80798b7e31362a57188879b5b8cb7c255fd47778c2e165c7c08b1133",
    "zh:cc7b73976e9246ede8731f096603c02b777715dc3478803d19ff99919040bfd7",
    "zh:d3290f84b776e1f6c2fee1c0a56b80fcc7d8447c33613ba955cf9ff5de0ac768",
    "zh:f8151a92f0169f788612a00851061c30eb049c3b1230c907cd2b0702fa1c0dbd",
  ]
}

provider "registry.terraform.io/terraform-provider-openstack/openstack" {
  version     = "1.35.0"
  constraints = "~> 1.35.0"
  hashes = [
    "h1:k1SCosvSICWAgRkswl83KtCycN7iP9asejWDDEQEtuk=",
    "zh:04cf8800c83289a28619ac9925bc03e0ccd624f0ed68284f8bd473cf48f05ef0",
    "zh:15fc2e1ea6f87d11e15aad075f3bfb7013eb63f31637f1ee317c94686c9650ec",
    "zh:1b6625ce80e6d8f192c984dcf6ba7ab303e4fdab6fa5a0a5651a8a01521aa879",
    "zh:4eb60013433f45fa3ef3fca314f68202e40ea3a12b0a5ccf75d8708002bbd8cd",
    "zh:505a7b22c813874090ceaee1a3e7f29961d0fe5a854d90f313aec5aa4900f44a",
    "zh:7be239b7e5672bd8fc3f3744bfe58dff73f8317ede349228a92dbd92b291a832",
    "zh:97b21c64da2700a69b5a3d30e514d76b52fc1089a45a0e02611aadb071b6fcc1",
    "zh:b45be0621b08f16236893a5bcc0e7fa1552176b299f6dbafa806f8b0ba5e4096",
    "zh:cd3b4387766ab33fbf504b22e73196d5984bc647f5c8ac9483ad604cf5cd2ceb",
    "zh:e3b69afe0cab18521ee11e283f783e76c02d248de40f7a25d54576f2b1643003",
    "zh:f43d52db66544065040b40db6334a1dfe6c9084d87104af1b1d133f90bbf3a66",
  ]
}
