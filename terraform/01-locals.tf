# list of locals (computed)

locals  {
    CLUSTER_REGIONS = coalesce(var.NETWORKS_CLUSTER_REGIONS, toset(var.SWARM_INSTANCES_SPECIFICATIONS[*].region))
    KEYPAIR_NAME = "${var.PROJECT_NAME}_initial_op_keypair"

    OS_CLUSTER_REGIONS = var.NETWORK_PROVIDER_TYPE == "openstack" ? local.CLUSTER_REGIONS : []
    OVH_CLUSTER_REGIONS = var.NETWORK_PROVIDER_TYPE == "ovh" ? local.CLUSTER_REGIONS : []
}