#!/usr/bin/fish

# To use an Openstack cloud you need to authenticate against keystone, which
# returns a **Token** and **Service Catalog**. The catalog contains the
# endpoint for all services the user/tenant has access to - including nova,
# glance, keystone, swift.
#
set -x OS_AUTH_URL "https://auth.cloud.ovh.net/v3/"
set -x OS_IDENTITY_API_VERSION 3

set -x OS_USER_DOMAIN_NAME "$OS_USER_DOMAIN_NAME" || set -x OS_USER_DOMAIN_NAME "Default"
set -x OS_PROJECT_DOMAIN_NAME "$OS_PROJECT_DOMAIN_NAME" || set -x OS_PROJECT_DOMAIN_NAME "Default"

# With the addition of Keystone we have standardized on the term **tenant**
# as the entity that owns the resources.
set -x OS_TENANT_ID ""
set -x OS_TENANT_NAME ""

# In addition to the owning entity (tenant), openstack stores the entity
# performing the action as the **user**.
set -x OS_USERNAME "user-"

# With Keystone you pass the keystone password.
echo "Please enter your OpenStack Password: "
read -s OS_PASSWORD_INPUT
set -x OS_PASSWORD "$OS_PASSWORD_INPUT"

# If your configuration has multiple regions, we set that information here.
# OS_REGION_NAME is optional and only valid in certain environments.
set -x OS_REGION_NAME "GRA"

# Don't leave a blank variable, unset it if it was empty
if test -z "$OS_REGION_NAME"
  set -e OS_REGION_NAME
end

# Terraform state backend configuration
set -x OS_TF_STATE_SWIFT_CONTAINER "tfstate-deepcube-wp2-swarm"
set -x OS_TF_STATE_SWIFT_ARCHIVE_CONTAINER "tfstate-deepcube-wp2-swarm-archive"
set -x OS_TF_STATE_SWIFT_REGION_NAME "GRA"

# generate the version terraform file and other conditional terraform files
set -l SCRIPT (status filename)
set -l SCRIPTPATH (dirname "$SCRIPT")
jinja2 "$SCRIPTPATH/01-versions.tf.j2" > "$SCRIPTPATH/01-versions.tf"
jinja2 "$SCRIPTPATH/00-providers.tf.j2" > "$SCRIPTPATH/00-providers.tf"


# export to TF / OVH

set -x TF_VAR_OS_AUTH_URL "$OS_AUTH_URL"
set -x TF_VAR_OS_USER_DOMAIN_NAME "$OS_USER_DOMAIN_NAME"
set -x TF_VAR_OS_PROJECT_DOMAIN_NAME "$OS_PROJECT_DOMAIN_NAME"
set -x TF_VAR_OS_TENANT_ID "$OS_TENANT_ID"
set -x TF_VAR_OS_TENANT_NAME "$OS_TENANT_NAME"
set -x TF_VAR_OS_USERNAME "$OS_USERNAME"
set -x TF_VAR_OS_PASSWORD "$OS_PASSWORD"
set -x TF_VAR_OS_REGION_NAME "$OS_REGION_NAME"