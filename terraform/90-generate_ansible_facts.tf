# generate the output files of the infrastructure

resource "local_file" "ansible_inventory" {

  count    = length(module.deepcube_swarm_cluster)

  filename = "${path.module}/../host_vars/generated/${module.deepcube_swarm_cluster[count.index].instance.name}.yml"
  content  = templatefile("${path.module}/templates/ansible_inventory.yaml.tpl", {
    instance         = module.deepcube_swarm_cluster[count.index].instance
    volumes          = module.deepcube_swarm_cluster[count.index].volumes
    volumes_attached = module.deepcube_swarm_cluster[count.index].volume_attachments
  })

  directory_permission  = 0700
  file_permission       = 0600
}